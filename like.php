<?php
require_once 'app/init.php';


if(isset($_GET['type'], $_SESSION['user_id'])){

	$user_id = (int)$_SESSION['user_id'];
	$article_id = (int)$_GET['id'];
	$type = $_GET['type'];

	switch($type){

		case'article':

		$params = [':userid' => $user_id, ':articleid' => $article_id];

		$ins = $DB->prepare('
			INSERT INTO articles_likes(`user_id`, `article_id`)
			
			SELECT :userid, :articleid
			FROM articles 
			WHERE EXISTS (

				SELECT id FROM articles
				WHERE id = :articleid

			)
			AND NOT EXISTS(

				SELECT id
				FROM articles_likes
				WHERE user_id = :userid
				AND
				      article_id = :articleid
			)

			LIMIT 1
			');


		if($ins->execute($params)){

			header('Location: index.php');
		}


		break;
	}
}


