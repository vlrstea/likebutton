<?php 
require_once 'app/init.php';

$articlesQuery = $DB->query("


	SELECT 

	articles.id, 
	articles.title, 
	COUNT(articles_likes.id) AS likes, 
	GROUP_CONCAT(users.username SEPARATOR '|') AS liked_by

	FROM articles
	
	LEFT JOIN articles_likes 
	ON (articles.id = articles_likes.article_id)
	LEFT JOIN users 
	ON (articles_likes.user_id = users.id)
	
	GROUP BY articles.id
	");
$articles = $articlesQuery->fetchAll(PDO::FETCH_OBJ);

foreach($articles as $article):

	$article->liked_by = ($article->liked_by) ? explode('|', $article->liked_by) : [];

	?>

	<div class="article">

		<h2><?php echo $article->title; ?></h2>
		<p>
			<a href="like.php?type=article&id=<?php echo $article->id; ?>">Like</a> 
			<?php echo $article->likes; ?> people liked this
		</p>
		<ul>

			<?php foreach($article->liked_by as $user): ?>

			<li><?= $user; ?></li>

		<?php endforeach; ?>
	</ul>

</div>


<?php endforeach;